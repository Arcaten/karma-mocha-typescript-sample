## Steps to Reproduce

* Clone repository
* Install npm

Then while in the repository's directory:
* npm install
* typings install

And either:
* gulp

or
* karma start karma.conf.js

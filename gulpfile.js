/* eslint-env node */

const karmaServer = require('karma').Server;
const gulp = require('gulp');
const path = require('path');

gulp.task('default', ['test']);

gulp.task('test', test);

gulp.task('tdd', tdd);

///////////

function test(done) {
    new karmaServer({
        configFile: path.resolve('./karma.conf.js'),
        singleRun: true
    }, done()).start();
};

function tdd(done) {
    new karmaServer({
        configFile: path.resolve('./karma.conf.js')
    }, done()).start();
};
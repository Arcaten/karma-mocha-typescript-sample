import * as mocha from 'mocha';
import * as chai from 'chai';
import Calculator from 'Calculator';

const expect = chai.expect;

describe('Calculator', function() {
    describe('#add', function() {
        it('should add two numbers', function () {
            let calc = new Calculator();
            expect(calc.add(1, 2)).to.equal(3);
        });
    });
});
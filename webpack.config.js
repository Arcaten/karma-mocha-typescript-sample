module.exports = {
    entry: "./src/Calculator.ts",
    output: {
        filename: "./bundle.js"
    },

    // Enable sourcemaps for debugging the webpack output
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension should be
            // handled by 'ts-loader'
            { test: /\.tsx?$/, loader: "ts-loader" }
        ],

        preLoaders: [
            // All output '.js' files will have sourcemaps re-processsed
            // by 'source-map-loader'
            { test: /\.js$/, loader: "source-map-loader" }
        ]
    }
};